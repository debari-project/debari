import { Injectable } from '@angular/core';

import { Geolocation } from '@ionic-native/geolocation';
import { AwaitProvider } from './../await/await';

@Injectable()
export class GeolocationProvider {

  constructor(private geolocation: Geolocation, private _AWAIT: AwaitProvider) {

  }

  async getCurrentPosition() {
    /* provar cuando no se tiene ubicación exacta */
    
    let err, res;

    [err, res] = await this._AWAIT.to(this.geolocation.getCurrentPosition());
    
    if(err) return err;

    return res;
  }

}
