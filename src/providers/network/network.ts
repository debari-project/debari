import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Network } from '@ionic-native/network';

@Injectable()
export class NetworkProvider {
  disconnectSubscription: any;
  connectSubscription: any;

  constructor(public network: Network, public eventCtrl: Events) { }

  public initializeNetworkEvents(): void {
    this.disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.eventCtrl.publish('network:offline');
    });

    this.connectSubscription = this.network.onConnect().subscribe(() => {
      this.eventCtrl.publish('network:online');
    });
  }

  public stopNetworkEvents(): void {
    this.disconnectSubscription.unsubscribe();
    this.connectSubscription.unsubscribe();
  }

  public getConnectionType(): string {
    return this.network.type;
  }
}
