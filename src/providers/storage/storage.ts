import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file';

import firebase from 'firebase/app';

@Injectable()
export class StorageProvider {

  constructor(public file: File) {

  }

  uploadUserImage(userId: string, imageString: any, source: string): Promise<any> {
    let image: string = 'user-' + userId + '.jpg',
      storageRef: any,
      usersRef: any,
      parseUpload: any;

    storageRef = firebase.storage().ref('users/' + userId + '/' + image);

    return new Promise((resolve, reject) => {

      if (source == 'local') {
        let data: string = this.file.applicationDirectory + "www/assets/imgs";
        this.file.readAsDataURL(data, "default-profile.jpg")
          .then(result => {
            resolve(storageRef.putString(result, 'data_url'));
          })
          .catch(err => {
            reject(err);
          });
      } else if (source == 'library') {

        storageRef.put(imageString.blob)
          .then(result => {
            resolve(result);
          })
          .catch((err)=>{
            reject(err);
          });

      }
    });
  }

  uploadImage(dir: string, imageString: Array<any>): Promise<any> {
    let ref = firebase.storage().ref(dir);

    return new Promise((resolve, reject) => {
      let dbPromises = [];

      imageString
        .forEach((doc: any) => {
          let image: string = 'alert-' + new Date().getTime() + "." + doc.type;
          dbPromises.push(ref.child(image).put(doc.blob));
        });

      Promise.all(dbPromises)
        .then(values => {
          resolve(values);
        })
        .catch((err) => {
          reject(err);
        });
    });

  }

  getUserImage(userId): Promise<any> {
    let image: string = 'user-' + userId + '.jpg',
      storageRef: any,
      parseUpload: any;

    return new Promise((resolve, reject) => {
      storageRef = firebase.storage().ref('users/' + userId + '/' + image);

      parseUpload = storageRef.getDownloadURL()
        .then(snapshot => {
          resolve(snapshot);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  getUserThumbnail(userId): Promise<any> {
    let image: string = 'thumb_user-' + userId + '.jpg',
      storageRef: any,
      parseUpload: any;

    return new Promise((resolve, reject) => {
      storageRef = firebase.storage().ref('users/' + userId + '/' + image);

      parseUpload = storageRef.getDownloadURL()
        .then(snapshot => {
          resolve(snapshot);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  deleteImages(imageString: Array<any>): Promise<any> {

    return new Promise((resolve, reject) => {
      let dbPromises = [];

      imageString
        .forEach((doc: any) => {
          let ref = firebase.storage().ref('alertImgs/' + doc.src);
          dbPromises.push(ref.delete());
        });

      Promise.all(dbPromises)
        .then(values => {
          resolve(0);
        })
        .catch((err) => {
          reject(-1);
        });

    });

  }
}
