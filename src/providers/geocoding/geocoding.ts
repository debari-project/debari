import { Injectable } from '@angular/core';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { AwaitProvider } from './../await/await';

@Injectable()
export class GeocodingProvider {

  constructor(private nativeGeocoder: NativeGeocoder, private _AWAIT: AwaitProvider) {
  }

  async reverseGeocode(lat, lng) {
    let err, res;

    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    [err, res] = await this._AWAIT.to(this.nativeGeocoder.reverseGeocode(lat, lng, options));

    if(err) return err;

    return res;
  }

/*
  async geocodeLatLng(lat, lng) {

    let latlng = {lat: lat, lng: lng};
    let err, res;

    [err, res] = await this._AWAIT.to(this.geocoder.geocode({'location': latlng}));

    if(err) return err;

    return res;
  }
*/

}
