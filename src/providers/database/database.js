var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
import 'firebase/firestore';
/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DatabaseProvider = (function () {
    function DatabaseProvider() {
        this._DB = firebase.firestore();
    }
    DatabaseProvider.prototype.searchEmail = function (userId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._DB
                .collection("users")
                .where("userId", "==", userId)
                .get()
                .then(function (querySnapshot) {
                var obj = [];
                querySnapshot
                    .forEach(function (doc) {
                    obj.push({
                        userId: doc.userId
                    });
                });
                resolve(obj);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.insertUser = function (userData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._DB.collection("users").add(userData)
                .then(function (obj) {
                resolve(obj);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    return DatabaseProvider;
}());
DatabaseProvider = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [])
], DatabaseProvider);
export { DatabaseProvider };
//# sourceMappingURL=database.js.map