import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Crop } from '@ionic-native/crop';

@Injectable()
export class ImageProvider {

  constructor(private _CAMERA: Camera, private file: File, private crop: Crop, public events: Events) { }

  async takePicture(sourceType: number, mediaType: number, crop: boolean = false): Promise<any> {

    let options: CameraOptions = {
      sourceType: sourceType,
      destinationType: this._CAMERA.DestinationType.FILE_URI,
      quality: 100,
      mediaType: mediaType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }, err, res, data, newImage;

    [err, data] = await this.to(this._CAMERA.getPicture(options));
    
    if (err) Promise.reject(err);

    if(data == null) {
      return Promise.resolve(true);
    }
    else {
      this.events.publish('file:loading');
      if (data.indexOf("file://") != 0) {
        data = "file://" + data;
      }
    
      if (crop) {
        [err, newImage] = await this.to(this.crop.crop(data));
        if (err) Promise.reject(err);
      
        [err, res] = await this.to(this.resolveLocalFilesystemUrl(newImage));
        if (err) Promise.reject(err);

        return Promise.resolve(res);
      }
      else {
        [err, res] = await this.to(this.resolveLocalFilesystemUrl(data));
        if (err) return Promise.reject(err);
        this.events.publish('file:loaded');

        return Promise.resolve(res);
      }
    }
  }
  
  resolveLocalFilesystemUrl(data): Promise<any> {

    return new Promise((resolve, reject) => {
      this.file.resolveLocalFilesystemUrl(data)
        .then((FE: any) => {
  
        FE.file(file => {
          if (file.size <= 26214400) {
          const FR = new FileReader();
  
          FR.onloadend = ((res: any) => {
            let ext = this.getFileExt(data);
            let AF = res.target.result;
            let blob = new Blob([new Uint8Array(AF)], { type: file.type });
            resolve({ "blob": blob, "src": data, "type": ext, name: file.name });
          });
  
          FR.readAsArrayBuffer(file);
          }
          else {
            reject('Sólo se pueden subir archivos de hasta 25MB');
          }
        },
        err => {
          reject(err);
        });
  
      });
    });

  }

  getFileExt(file: string): string {
    let type = file.substr(file.lastIndexOf('.') + 1);
    let last = type.indexOf('?');
    if (last == -1)
      last = type.length;
    type = type.substr(0,last);
	  
	  return type;
  }

  to(promise) {
    return promise.then(data => {
       return [null, data];
    })
    .catch(err => [err]);
  }

}
