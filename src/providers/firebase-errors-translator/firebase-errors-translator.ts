import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the FirebaseErrorsTranslatorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseErrorsTranslatorProvider {

  constructor(public http: HttpClient) {
    console.log('Hello FirebaseErrorsTranslatorProvider Provider');
  }

}
