import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AwaitProvider } from '../await/await';

import moment from 'moment';

@Injectable()
export class LocalDatabaseProvider {
  public displayName: string;

  constructor(public sqlite: SQLite, private _AWAIT: AwaitProvider) {

    this.getUserData()
      .then((result) => {
        if(result.display_name) {
          this.displayName = result.display_name;
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  openDatabase(): Promise<any> {
    return this.sqlite.create({ name: 'data.db', location: 'default' });
  }

  executeSql(db: any, command: string, parameters: any): Promise<any> {
    return new Promise((resolve, reject) => {
      db.then((db: SQLiteObject) => {
        return db.executeSql(command, parameters)
          .then((result) => {
            resolve(result);
          })
          .catch(err => reject(err));
      })
      .catch(err => reject(err));
    });
  }

  saveUserProfile(userObj): Promise<any> {
    let uid: string = userObj.uid;
    let displayName: string = userObj.displayName;
    let email: string = userObj.email;
    let photoURL: string = userObj.photoURL;
    let thumbnail: string = userObj.thumbnail;
    let creationTime: string = userObj.creationTime;
    let userCountryCode: string = userObj.userCountryCode || null;
    let range: string = userObj.range;
    let points: number = userObj.points;
    let alertsCount: number = userObj.alertsCount;
    
    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'INSERT INTO user (uid, display_name, email, photo_url, thumbnail, creation_time, user_country_code, range, points, alerts_count) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        parameters: any = [uid, displayName, email, photoURL, thumbnail, creationTime, userCountryCode, range, points, alertsCount];

      this.executeSql(db, command, parameters)
        .then((result) => {
          console.log('ok guardado');
          resolve(true);
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });

    });
  }

  async insertChatRoom(roomObj): Promise<any> {
    let err, result;

    let id = roomObj.id;
    let buddyName;
    let lmDate;

    if(roomObj.buddyId == null) {
      let userData = await this._AWAIT.to(roomObj.userData);
      buddyName = userData[1].displayName;
      lmDate = roomObj.date;
    }
    else {
      buddyName = roomObj.buddyName;
      lmDate = new Date().getTime();
    }

    let buddyId = roomObj.buddyId;
    let lastMessage = roomObj.message;

    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'INSERT INTO chat_rooms (id, buddy_uid, buddy_name, last_message, lm_date, unseen_msgs) VALUES(?, ?, ?, ?, ?, ?)',
        parameters: any = [id, buddyId, buddyName, lastMessage, lmDate, 1];

      this.executeSql(db, command, parameters)
        .then((result) => {
          console.log('ok guardado chat room');
          resolve(true);
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });
    });
  }
/*
  async updateChatRoom(roomObj): Promise<any> {
    let err, result;

    let id = roomObj.id;
    let userData = await this._AWAIT.to(roomObj.userData);
    let buddyId = roomObj.buddyId;
    let buddyName = userData[1].displayName;
    let lastMessage = roomObj.message;
    let lmDate = roomObj.date;

    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'INSERT INTO chat_rooms (id, buddy_uid, buddy_name, last_message, lm_date) VALUES(?, ?, ?, ?, ?)',
        parameters: any = [id, buddyId, buddyName, lastMessage, lmDate];

      this.executeSql(db, command, parameters)
        .then((result) => {
          console.log('ok guardado chat room');
          resolve(true);
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });
    });
  }
*/
  async insertPrivateMessage(roomId, msgObj): Promise<any> {
    let err, result;

    let id = msgObj.msgId;
    let message = msgObj.message;
    let date = msgObj.date.getTime();
    console.log(date);
    let senderId = msgObj.senderId;

    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'INSERT INTO chat_messages (id, room_id, message, date, sender_id, sent, viewed) VALUES(?, ?, ?, ?, ?, ?, ?)',
        parameters: any = [id, roomId, message, date, senderId, true, false];

      this.executeSql(db, command, parameters)
        .then((result) => {
          console.log('ok guardado mensaje guardado');
          resolve(true);
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });

    });
  }

  async getMessages(roomId): Promise<any> {
    let err, result, results = [];
    let db: any = this.openDatabase(),
    command: string = 'SELECT id, room_id, message, date, sender_id, sent, viewed FROM chat_messages WHERE room_id = ? ORDER BY date',
    parameters: any = [roomId];

    [err, result] = await this._AWAIT.to(this.executeSql(db, command, parameters));

    if(err) return Promise.reject(err);
    
    for (let i = 0; i < result.rows.length; i++) {
      let item = {};

      item["id"] = result.rows.item(i).id;
      item["message"] = result.rows.item(i).message;
      item["date"] = result.rows.item(i).date;
      item["senderId"] = result.rows.item(i).sender_id;
      item["sent"] = result.rows.item(i).sent;
      item["viewed"] = result.rows.item(i).viewed;

      // do something with it

      results.push(item);
    }

    return Promise.resolve(results);
  }

  async getChatRooms(): Promise<any> {
    let err, result, results = [];
    let db: any = this.openDatabase(),
    command: string = 'SELECT id, buddy_uid, buddy_name, last_message, lm_date, unseen_msgs FROM chat_rooms ORDER BY lm_date DESC',
    parameters: any = {};

    [err, result] = await this._AWAIT.to(this.executeSql(db, command, parameters));

    if(err) return Promise.reject(err);
    
    for (let i = 0; i < result.rows.length; i++) {
      let item = {};

      item["id"] = result.rows.item(i).id;
      item["buddyId"] = result.rows.item(i).buddy_uid;
      item["buddyName"] = result.rows.item(i).buddy_name;
      item["message"] = result.rows.item(i).last_message;
      item["date"] = result.rows.item(i).lm_date;
      item["unseenMsgs"] = result.rows.item(i).unseen_msgs;

      // do something with it

      results.push(item);
    }

    console.log(results);

    return Promise.resolve(results);
  }

  pipeDate(pDate) {
    moment.locale('es');

    const currentDate = moment().format('DD/MM/YY');
    const date = moment(pDate).format('DD/MM/YY');
    
    if(currentDate == date) {
      const hour = moment(pDate).format("HH:mm");
      return hour;
    }
    else {
      return date;
    }
  }

  async updateChatRoom(roomObj): Promise<any> {
    let id: string = roomObj.id;
    let buddyName;

    if(!roomObj["buddyName"]) {
      let userData = await this._AWAIT.to(roomObj.userData);
      buddyName = userData[1].displayName;
    } else {
      buddyName = roomObj.buddyName;
    }

    let lastMessage: string = roomObj.message;
    let date: string = roomObj.date.getTime();

    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'UPDATE chat_rooms SET buddy_name = ?, last_message = ?, lm_date = ?, unseen_msgs = unseen_msgs + 1 WHERE id = ?',
        parameters: any = [buddyName, lastMessage, date, id];

      this.executeSql(db, command, parameters)
		.then(result=> {
			resolve(true);
		})
		.catch(err=>{
			return(err);
		});

    });
  }

  async markMessagesAsViewed(roomId): Promise<any> {

    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'UPDATE chat_rooms SET unseen_msgs = 0 WHERE id = ?',
        parameters: any = [roomId];

      this.executeSql(db, command, parameters)
      .then(result=> {
        resolve(true);
      })
      .catch(err=>{
        return(err);
      });

    });
  }

  updateUserProfile(userObj): Promise<any> {
    let photoURL: string = userObj.photoURL;

    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'UPDATE user SET photo_url = ?',
        parameters: any = [photoURL];

      this.executeSql(db, command, parameters)
		.then(result=> {
			resolve(true)
		})
		.catch(err=>{
			return(err);
		});

    });
  }

  getUserData(): Promise<any> {
    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'SELECT uid, display_name, email, photo_url, thumbnail, creation_time, user_country_code, range, points, alerts_count FROM user',
        parameters: any = {};

      this.executeSql(db, command, parameters)
        .then((result) => {
          console.log(result);
          resolve(result.rows.item(0));
        })
        .catch(err => reject(err)); 
    });
  }

  deleteUserData() {
    let db: any = this.openDatabase(),
      command: string = 'DELETE FROM user',
      parameters: any = {};

    this.executeSql(db, command, parameters)
      .then((result) => {
        console.log('ok');
      })
      .catch((err) => {
        console.log(err);
      });
  }

  async deleteChatRoom(roomId): Promise<any> {
    let db: any = this.openDatabase(),
      command: string = 'DELETE FROM chat_rooms WHERE id = ? OR id IS NULL', //mientras lo de is null
      parameters: any = [roomId],
      err, result;

    [err, result] = await this._AWAIT.to(this.executeSql(db, command, parameters));
    if(err) return Promise.reject(err);

    command = 'DELETE FROM chat_messages WHERE room_id = ? OR id IS NULL';
    [err, result] = await this._AWAIT.to(this.executeSql(db, command, parameters));
    if(err) return Promise.reject(err);

    return Promise.resolve(result);
  }

  insertSearchPattern(pattern: string, isTag: boolean = false): Promise<any> {
    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'INSERT INTO search_history(pattern, is_tag) VALUES(?, ?)',
        parameters: any = [pattern, isTag];
 
      this.executeSql(db, command, parameters)
        .then((result) => {
          resolve(true);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
 
  getSearchHistory(): Promise<any> {
    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'SELECT id, pattern, is_tag FROM search_history ORDER BY id DESC LIMIT 10',
        parameters: any = {};
 
      this.executeSql(db, command, parameters)
        .then((result) => {
 
          let lists = [];
          for (let i = 0; i < result.rows.length; i++) {
            lists.push(result.rows.item(i));
          }

          resolve(lists);
        })
        .catch(err => { reject(err) });
    });
  }
  
  deleteSearchHistory(id): Promise<any> {
    return new Promise((resolve, reject) => {
      let db: any = this.openDatabase(),
        command: string = 'DELETE FROM search_history WHERE id = ?',
        parameters: any = [id];
 
      this.executeSql(db, command, parameters)
        .then((result) => {
          resolve(true);
        })
        .catch((err) => {
          reject(err);
        });
    })
  }

  getDisplayName() {
    return this.displayName;
  }
}
