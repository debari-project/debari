import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController, ModalController, Keyboard, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireAuth } from 'angularfire2/auth';
import { AuthProvider } from '../providers/auth/auth';
import { AlertProvider } from '../providers/alert/alert';
import { LocalDatabaseProvider } from '../providers/local-database/local-database';

import { Splash } from '../pages/splash/splash';
import { ImageLoaderConfig } from 'ionic-image-loader';

import { FCM } from '@ionic-native/fcm';
import { DatabaseProvider } from '../providers/database/database';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  uid: string;
  displayName: string;
  email: string;
  photoURL: string = 'assets/imgs/default-profile.jpg';
  pages: Array<{title: string, component: any, icon: string, uid: string}>;
  unseenReceivedAlerts: number = 0;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private afAuth: AngularFireAuth,
    public authData: AuthProvider,
    public events: Events,
    public menuCtrl: MenuController,
    public _ALERT: AlertProvider,
    public _LOCALDB: LocalDatabaseProvider,
    public modalCtrl: ModalController,
    public keyboard: Keyboard,
    private imageLoaderConfig: ImageLoaderConfig,
    private fcm: FCM,
    private _DB: DatabaseProvider,
    private alertCtrl: AlertController
  ) {

    this.imageLoaderConfig.enableDebugMode();
    this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
    this.imageLoaderConfig.setFallbackUrl('assets/imgs/default.png');
    this.imageLoaderConfig.setMaximumCacheAge(24 * 60 * 60 * 1000);
    this.imageLoaderConfig.setImageReturnType('base64');

    events.subscribe('user:loggedin', (uid, usuario, email, photoURL) => {      
      this.uid = uid;
      this.displayName = usuario;
      this.email = email;
      this.photoURL = photoURL;
    });

    events.subscribe('user:imagechanged', (photoURL) => {
      this.photoURL = photoURL;
    });

	events.subscribe('alert:resetAlertsUnseen', (result) => {
		    this._LOCALDB.executeSql(this._LOCALDB.openDatabase(), 'delete from unseen_alerts', {})
            .then(result => { console.log('ok') })
            .catch(err => { this._ALERT.presentAlert('err', err.message) });
	      this.unseenReceivedAlerts = 0;
	    });

    this.pages = [
      { title: 'Mi cuenta', component: 'AccountPage', icon: 'ios-contact', uid: this.uid },
      { title: 'Alertas creadas', component: 'CreatedAlertsPage', icon: 'md-open', uid: this.uid },
      { title: 'Alertas recibidas', component: 'ReceivedAlertsPage', icon: 'md-download', uid: this.uid },
      { title: 'Ayuda', component: 'HelpPage', icon: 'ios-help-buoy', uid: this.uid },
      { title: 'Acerca de', component: 'AboutPage', icon: 'ios-information-circle-outline', uid: this.uid },
      { title: 'Salir', component: 'LoginPage', icon: 'md-exit', uid: this.uid }
    ];

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
/*
      this._LOCALDB.executeSql(this._LOCALDB.openDatabase(),
      'DROP TABLE IF EXISTS user;', {})
        .then(() => console.log("se dropeo la tabla usuario"))
        .catch(e => {
          console.log(e);
          //this._ALERT.presentAlert("Error SQLite", e)
        });

*/

      this._LOCALDB.executeSql(this._LOCALDB.openDatabase(),
      `CREATE TABLE IF NOT EXISTS user(uid TEXT,
        display_name VARCHAR(30),
        user_id VARCHAR(30),
        user_id_insensitive VARCHAR(30),
        email TEXT,
        photo_url TEXT,
        thumbnail TEXT,
        creation_time TEXT,
        user_country_code TEXT,
        range TEXT,
        points INTEGER,
        alerts_count INTEGER)`, {})
          .then(() => console.log("se creo la tabla usuario"))
        .catch(e => {
          console.log(e);
          //this._ALERT.presentAlert("Error SQLite", e)
        });

      this._LOCALDB.executeSql(this._LOCALDB.openDatabase(),
        'CREATE TABLE IF NOT EXISTS search_history(id INTEGER PRIMARY KEY, pattern TEXT, is_tag BOOLEAN)', {})
        .then(() => console.log("se creo la tabla de historial"))
        .catch(e => {
          console.log(e);
          //this._ALERT.presentAlert("Error SQLite", e)
        });

      this._LOCALDB.executeSql(this._LOCALDB.openDatabase(),
            'CREATE TABLE IF NOT EXISTS unseen_alerts(alert_id VARCHAR(30))', {})
            .then(() => console.log("se creo la tabla unseen_alerts"))
          .catch(e => {
            console.log(e);
            //this._ALERT.presentAlert("Error SQLite", e)
          });

      this._LOCALDB.executeSql(this._LOCALDB.openDatabase(),

      //unique buddy_uid

          'CREATE TABLE IF NOT EXISTS chat_rooms(id TEXT PRIMARY KEY NOT NULL, buddy_uid TEXT NOT NULL, buddy_name TEXT, last_message TEXT, lm_date LONG, msg_count INTEGER, unseen_msgs INTEGER)', {})
          .then(() => console.log("se creo la tabla chat_rooms"))
        .catch(e => {
          console.log(e);
          //this._ALERT.presentAlert("Error SQLite", e)
        });

        this._LOCALDB.executeSql(this._LOCALDB.openDatabase(),
        'CREATE TABLE IF NOT EXISTS chat_messages(id TEXT PRIMARY KEY, room_id TEXT NOT NULL, message TEXT, date LONG, sender_id TEXT, sent BOOLEAN, viewed BOOELAN)', {})
        .then(() => console.log("se creo la tabla chat_rooms"))
        .catch(e => {
          console.log(e);
          //this._ALERT.presentAlert("Error SQLite", e)
        });

/*
        this._LOCALDB.executeSql(this._LOCALDB.openDatabase(),  
            'DELETE FROM chat_rooms; VACUUM', {})
            .then(() => console.log("se trunco chat_rooms"))
          .catch(e => {
            console.log(e);
          });

          this._LOCALDB.executeSql(this._LOCALDB.openDatabase(),  
          'DELETE FROM chat_messages; VACUUM', {})
          .then(() => console.log("se trunco chat_rooms"))
        .catch(e => {
          console.log(e);
        });

*/
      this._LOCALDB.executeSql(this._LOCALDB.openDatabase(), 'SELECT count(*) AS "unseen_alerts" FROM unseen_alerts', {})
            .then((result) => {
              this.unseenReceivedAlerts = result.rows.item(0).unseen_alerts;
            })
            .catch(e => {
              console.log(e);
              //this._ALERT.presentAlert("Error", e)
            });

            const authObserver = this.afAuth.authState.subscribe(user => {

              if (user && user.emailVerified) {
        
                this._LOCALDB.getUserData()
                  .then(result => {
                    console.log(result);
					
					if(result) {
						this.uid = result.uid;
						this.displayName = result.display_name;
						this.email = result.email;
						this.photoURL = result.photo_url;
			
						this.rootPage = 'HomePage';
						authObserver.unsubscribe();
					}
					else {
						this.rootPage = 'LoginPage';
					}

                    authObserver.unsubscribe();
                  });
        
              } else {
                this.rootPage = 'LoginPage';
                authObserver.unsubscribe();
              }
        
            });
  
      //Notifications

      this.fcm.subscribeToTopic('all');

      this.fcm.getToken().then(token=>{
          this._DB.saveToken(token)
          .then((result)=>{
            console.log(result);
          })
          .catch(err=>{
            console.log(err);
          });
      })
      this.fcm.onNotification().subscribe(data=>{
        
        if(data.wasTapped){
          this.unseenReceivedAlerts += 1;
          this._LOCALDB.executeSql(this._LOCALDB.openDatabase(), 'insert into unseen_alerts values("1")', {})
            .then(result => { console.log('ok') })
            .catch(err => { this._ALERT.presentAlert('err', err.message) });
        } else {
          this.unseenReceivedAlerts += 1;
          this._LOCALDB.executeSql(this._LOCALDB.openDatabase(), 'insert into unseen_alerts values("1")', {})
            .then(result => { console.log('ok')})
            .catch(err => { this._ALERT.presentAlert('err', err.message) });
        };
      })
      this.fcm.onTokenRefresh().subscribe(token=>{
        console.log(token);
      });

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      //this.splashScreen.hide();
      //let splash = this.modalCtrl.create(Splash);
      //splash.present();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

    if (page.component == "LoginPage") {
      let alertConfirmExit = this.alertCtrl.create({
        title: 'Confirmación',
        message: '¿Estás seguro que deseas salir de tu cuenta?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('cancel exit');
            }
          },
          {
            text: 'Ok',
            handler: () => {
              this.exitApp();
            }
          }
        ]
      });

      alertConfirmExit.present();

      return false;

    }
    else {
      this.nav.push(page.component, { uid: this.uid });
    }

  }

  exitApp() {
    this.authData.logoutUser();
    this._LOCALDB.deleteUserData();
    this.nav.setRoot('LoginPage');
  }
}
