import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { AppVersion } from '@ionic-native/app-version';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { SQLite } from '@ionic-native/sqlite';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ImagePicker } from '@ionic-native/image-picker';
import { StreamingMedia } from '@ionic-native/streaming-media';

import { Network } from '@ionic-native/network';
import { Geolocation } from '@ionic-native/geolocation';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { VideoEditor } from '@ionic-native/video-editor';
import { Crop } from '@ionic-native/crop';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Diagnostic} from '@ionic-native/diagnostic';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { MyApp } from './app.component';
import { FIREBASE_CONFIG } from './firebase.credentials';


import { AuthProvider } from '../providers/auth/auth';
import { AlertProvider } from '../providers/alert/alert';
import { DatabaseProvider } from '../providers/database/database';
import { StorageProvider } from '../providers/storage/storage';
import { PreloaderProvider } from '../providers/preloader/preloader';
import { ImageProvider } from '../providers/image/image';
import { LocalDatabaseProvider } from '../providers/local-database/local-database';
import { ToastProvider } from '../providers/toast/toast';
import { FirebaseErrorsTranslatorProvider } from '../providers/firebase-errors-translator/firebase-errors-translator';
import { NetworkProvider } from '../providers/network/network';

//import { Splash } from '../pages/splash/splash';

import { IonTagsInputModule } from "ionic-tags-input";
import { CalendarModule } from 'ionic3-calendar-en';
import { IonicImageLoader } from 'ionic-image-loader';
import { DirectivesModule } from '../directives/directives.module';
import { AwaitProvider } from '../providers/await/await';

import {FCM} from '@ionic-native/fcm';
import { GeolocationProvider } from '../providers/geolocation/geolocation';
import { GeocodingProvider } from '../providers/geocoding/geocoding';
import { AndroidPermissionsProvider } from '../providers/android-permissions/android-permissions';
import { DiagnosticProvider } from '../providers/diagnostic/diagnostic';

import { IonicStorageModule } from '@ionic/storage';
import { LocalStorageProvider } from '../providers/local-storage/local-storage';

@NgModule({
  declarations: [
    MyApp,
    //Splash

  ],
  imports: [
    BrowserModule,
	  IonTagsInputModule,
    IonicModule.forRoot(MyApp, {
      monthShortNames: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
      months : [
        "Enero", "Febrero", "March", "April", "May", "June", "July",
        "August", "September", "October", "Noviembre", "December"
      ],
      tabsHideOnSubPages: true,
      tabSubPages: false
    }),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
	  CalendarModule,
    IonicImageLoader.forRoot(),
    DirectivesModule,
    IonicStorageModule.forRoot()
  ],/*
  exports: [
    AutoHideDirective
  ],*/
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    //Splash
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppVersion,
    AuthProvider,
    AlertProvider,
    DatabaseProvider,
    Diagnostic,
    StorageProvider,
    File,
    PreloaderProvider,
    ImageProvider,
    Camera,
    SQLite,
    LocalDatabaseProvider,
    ToastProvider,
    ImagePicker,
    FirebaseErrorsTranslatorProvider,
    Network,
    NetworkProvider,
    Geolocation,
    PhotoViewer,
    LocationAccuracy,
    VideoEditor,
    Crop,
    StreamingMedia,
    AwaitProvider,
    FCM,
    InAppBrowser,
    AndroidPermissions,
    NativeGeocoder,
    GeolocationProvider,
    GeocodingProvider,
    AndroidPermissionsProvider,
    DiagnosticProvider,
    LocalStorageProvider
  ]
})
export class AppModule {}
