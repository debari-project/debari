import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'timeAgo',
})
export class TimeAgoPipe implements PipeTransform {

  transform(value: Date, ...args) {
    let now: any = new Date(), seconds, hours, minutes, days, month, year, hour, day;
    
    seconds = Math.floor((now.getTime() - value.getTime()) / 1000);
    minutes = Math.floor(seconds / 60);
    hours = Math.floor(minutes / 60);
    days = Math.floor(hours / 24);
    
    if(seconds < 60) {
      return 'hace unos segundos';
    }
    else if(minutes < 60) {
      return minutes + ' min';
    }
    else if(hours < 24) {
     return hours + ' h';
    }
    else {
      moment.locale('es');
      day = moment(value).date();

      month = moment(value).format('MMM');
      year = moment(value).year();
      hour = moment(value).format('HH:mm');
      return day + ' ' + month + ' ' + year + ' a las ' + hour;
    }
    }
}
