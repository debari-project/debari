import { Directive, Input, Renderer, ElementRef } from '@angular/core';

@Directive({
  selector: '[auto-hide]', // Attribute selector
  host: {
    '(ionScroll)': 'onContentScroll($event)'
  }
})
export class AutoHideDirective {
  @Input("header") header: HTMLElement;
  fabToHide;
  oldScrollTop: number = 0;
  headerHeight;
  scrollContent;

  constructor(private renderer: Renderer, private element: ElementRef) {

  }

  ngOnInit() {
    this.fabToHide = this.element.nativeElement.getElementsByClassName("fab")[0];
    this.headerHeight = this.header.clientHeight;
    this.renderer.setElementStyle(this.fabToHide, "webkitTransition", "transform 500ms, opacity 500ms");
    //this.renderer.setElementStyle(this.header, 'webkitTransition', 'top 100ms');
    this.scrollContent = this.element.nativeElement.getElementsByClassName("scroll-content")[0];
    //this.renderer.setElementStyle(this.scrollContent, 'webkitTransition', 'margin-top 100ms');
  }

  onContentScroll(e) {
    /*if (e.scrollTop - this.oldScrollTop > 10) {
      this.renderer.setElementStyle(this.fabToHide, "opacity", "0");
      this.renderer.setElementStyle(this.fabToHide, "webkitTransform", "scale3d(.1,.1,.1)");
    } else if (e.scrollTop - this.oldScrollTop < 0) {
      this.renderer.setElementStyle(this.fabToHide, "opacity", "1");
      this.renderer.setElementStyle(this.fabToHide, "webkitTransform", "scale3d(1,1,1)");
    }*/

    if (e.scrollTop - this.oldScrollTop > 10) {
      this.renderer.setElementStyle(this.header, "top", "-56px");
      this.renderer.setElementStyle(this.scrollContent, "margin-top", "0px");
      this.renderer.setElementStyle(this.fabToHide, "opacity", "0");
      this.renderer.setElementStyle(this.fabToHide, "webkitTransform", "scale3d(.1,.1,.1)");
    } else if (e.scrollTop - this.oldScrollTop < 0)  {
      this.renderer.setElementStyle(this.header, "top", "0px");
      this.renderer.setElementStyle(this.scrollContent, "margin-top", "56px");
      this.renderer.setElementStyle(this.fabToHide, "opacity", "1");
      this.renderer.setElementStyle(this.fabToHide, "webkitTransform", "scale3d(1,1,1)");
    }

    this.oldScrollTop = e.scrollTop;
  }
}
