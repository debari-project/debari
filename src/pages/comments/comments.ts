import { Component, ViewChild, ElementRef } from '@angular/core';
import {
  IonicPage, NavController, NavParams,
  PopoverController, Events, Content, InfiniteScroll } from 'ionic-angular';

import { LocalDatabaseProvider } from '../../providers/local-database/local-database';
import { DatabaseProvider } from '../../providers/database/database';
import { AlertProvider } from '../../providers/alert/alert';
import { StorageProvider } from '../../providers/storage/storage';

@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {
  @ViewChild('myInput') myInput: ElementRef;
  @ViewChild(Content) content: Content;
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;

  private comments: Array<any> = [];
  private comment: string = '';
  private uid: string;
  private displayName: string;
  private alertId: string;
  private commentId: string;
  private photoURL: string;
  public lastComment: string;
  private offset: string = null;
  private loading: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public _DB: DatabaseProvider,
    public _LOCALDB: LocalDatabaseProvider,
    public _ALERT: AlertProvider,
    private popoverCtrl: PopoverController,
    private _STORAGE: StorageProvider,
    private events: Events) {
    events.subscribe('comment:deleted', (commentId) => {
      this.comments.forEach((doc, index) => {
        if (doc.id == commentId) this.comments.splice(index, 1);
      });
    });
  }

  ionViewDidEnter() {
    this._LOCALDB.getUserData()
      .then(result => {
        this.uid = result.uid;
        this.displayName = result.display_name;
        this.photoURL = result.photo_url;
      });

    this.alertId = this.navParams.get('alertId');
    this.commentId = this.navParams.get('commentId');

    this.loadComments()
    .then(()=>{
      if(this.commentId != null) {
        this.scrollTo(this.commentId);
      }
    })
    .catch((err)=>{

    });
  }

  scrollTo(elementId: string) {

    let y = document.getElementById(elementId).offsetTop;	

    this.content.scrollTo(0, y);
  }

  loadComments(): Promise<any> {
    return new Promise((resolve) => {
      console.log("this.offset: " + this.offset);
      this._DB.getAlertComments({ alertId: this.alertId, offset: this.offset })
        .then((data) => {
          if (data.length > 0) {
            this.comments = this.comments.concat(data);
            this.offset = data[data.length - 1].commentDate;
          }
          else {
            this.infiniteScroll.enabled = false;
          }
          this.loading = false;
          resolve(true);
        })
        .catch(err => {
          this._ALERT.presentAlert('Error', err.messsage);
        });
    });
  }

  addComment() {
    let obj: any = { alertId: this.alertId, comment: this.comment };
    this.comment = '';

    this._DB.addComment(obj)

      .then(result => {

        obj = result;
        obj["displayName"] = this.displayName;

        this.comments.push(obj);
      })
      .catch(err => {
        this._ALERT.presentAlert('Error', err.message);
      });
  }

  elapsedTime(pDate): string {
    let date: any = new Date(pDate),
      now: any = new Date(), diff, hours, minutes, seconds, elapsedTime;

    diff = Math.floor(now - date);

    if (diff < 86400000) {
      hours = Math.floor(diff / 3600000);

      if (hours > 0) {
        elapsedTime = hours + " h";
      }
      else {

        minutes = Math.floor(diff / 60000);

        if (minutes > 0) {
          elapsedTime = minutes + " min";
        }
        else {
          elapsedTime = " un momento";
        }
      }

      elapsedTime = "hace " + elapsedTime;
    }
    else {
      let dateString = pDate.toString();
      //elapsedTime = this.formatDate(dateString.substr(0, dateString.indexOf('GMT') - 4));
      elapsedTime = dateString.substr(0, dateString.indexOf('GMT') - 4);
    }

    return elapsedTime;
  }

  formatDate(pDate): string {
    let day, month, numMonth, year, hour, date;

    year = pDate.slice(0, 4);
    numMonth = Number(pDate.slice(5, 7));
    switch (numMonth) {
      case 1:
        month = "ene";
        break;
      case 2:
        month = "feb";
        break;
      case 3:
        month = "mar";
        break;
      case 4:
        month = "abr";
        break;
      case 5:
        month = "may";
        break;
      case 6:
        month = "jun";
        break;
      case 7:
        month = "jul";
        break;
      case 8:
        month = "ago";
        break;
      case 9:
        month = "set";
        break;
      case 10:
        month = "oct";
        break;
      case 11:
        month = "nov";
        break;
      case 12:
        month = "dic";
        break;
    }

    day = pDate.slice(8, 10);
    hour = pDate.slice(11, 17);

    date = day + " " + month + ". " + year + " " + hour;

    return date;
  }

  presentPopover(ev, comment) {
    let options: any = [];

    if (comment.uid == this.uid) {
      options.push({ optionName: 'Eliminar', component: 'DeleteComment', comment: comment });

      let popover = this.popoverCtrl.create('CommentsPopoverPage', {
        options: options
      });

      popover.present({
        ev: ev
      });

    }
  }

  resize() {

    if (this.myInput.nativeElement.scrollHeight <= 61) {
      this.myInput.nativeElement.style.height = this.myInput.nativeElement.scrollHeight + 'px';
    }
  }

  doInfinite(infiniteScroll) {
    infiniteScroll.enable(false);

    this.loadComments()
      .then(result => {
        infiniteScroll.complete();

        if (this.lastComment != this.comments[0].id) {
          this.lastComment = this.comments[0].id;
          infiniteScroll.enable(true);
        }

      });
  }
  /*
  presentModal(page, params) {
    let modal = this.modalCtrl.create(page, params, { cssClass: 'comments-modal' });
    modal.present();
  }
  */

}
