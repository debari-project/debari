import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserNewsFeedPage } from './user-news-feed';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { DirectivesModule } from '../../directives/directives.module';
import { IonicImageLoader } from 'ionic-image-loader';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    UserNewsFeedPage,
  ],
  imports: [
    IonicPageModule.forChild(UserNewsFeedPage),
    TruncateModule,
	  IonicImageLoader,
    DirectivesModule,
    PipesModule,
	  ComponentsModule
  ],
})
export class UserNewsFeedPageModule {}
