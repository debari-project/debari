import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedAlertsPage } from './received-alerts';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { DirectivesModule } from '../../directives/directives.module';
import { IonicImageLoader } from 'ionic-image-loader';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    ReceivedAlertsPage,
  ],
  imports: [
  IonicPageModule.forChild(ReceivedAlertsPage),
    TruncateModule,
	  IonicImageLoader,
    DirectivesModule,
    PipesModule
  ],
})
export class ReceivedAlertsPageModule {}
