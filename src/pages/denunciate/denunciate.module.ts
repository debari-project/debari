import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DenunciatePage } from './denunciate';

@NgModule({
  declarations: [
    DenunciatePage,
  ],
  imports: [
    IonicPageModule.forChild(DenunciatePage),
  ],
})
export class DenunciatePageModule {}
