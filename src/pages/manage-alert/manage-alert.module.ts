import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageAlertPage } from './manage-alert';
import { IonTagsInputModule } from "ionic-tags-input";
import { CalendarModule } from 'ionic3-calendar-en';

@NgModule({
  declarations: [
    ManageAlertPage,
  ],
  imports: [
	IonTagsInputModule,
    IonicPageModule.forChild(ManageAlertPage),
	CalendarModule
  ],
})
export class ManageAlertPageModule {}
