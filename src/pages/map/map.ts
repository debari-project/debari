import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

import { NetworkProvider } from '../../providers/network/network';
import { Geolocation } from '@ionic-native/geolocation';
import { AlertProvider } from '../../providers/alert/alert';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';

import { PreloaderProvider } from '../../providers/preloader/preloader';

declare var google;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  markers: any = [];
  latLng: any;
  lat: number;
  lng: number;
  locationSelected: boolean = false;
  okButtonEnable: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    private geolocation: Geolocation,
    public _NETWORK: NetworkProvider,
    public _ALERT: AlertProvider,
    private locationAccuracy: LocationAccuracy,
    private _PRELOADER: PreloaderProvider,
    private androidPermissions: AndroidPermissions) {
    this.lat = this.navParams.get('lat');
    this.lng = this.navParams.get('lng');
  }

  ionViewDidLoad() {

    this.map = null;
    
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if(!result.hasPermission) {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then((requestResult) =>{

            if(requestResult.hasPermission) {

              this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                if (canRequest) {
                  // the accuracy option will be ignored by iOS
                  this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                    () => {
                      if (!this.lat) {
                        this.geolocation.getCurrentPosition().then((resp) => {
                          this.lat = resp.coords.latitude;
                          this.lng = resp.coords.longitude;
                          this.loadMap();
                        }).catch((err) => {
                          this._ALERT.presentAlert('Error', err.message);
                        });
                      }
                      else {
                        this.locationSelected = true;
                        this.loadMap();
                      }
                    },
                    error => {
                      this.lat = -25.30066;
                      this.lng = -57.63591;
                      this.loadMap();
                    }
                  );
                }
                else {
                  this._ALERT.presentAlert('Error', 'Verifica que Debari pueda utilizar tu gps en tus ajustes');
                }
              });
            }
            else {
              this.lat = -25.30066;
              this.lng = -57.63591;
              this.loadMap();    
            }
          })
          .catch((err) =>{
            this.lat = -25.30066;
            this.lng = -57.63591;
            this.loadMap();
          });
        }
        else {
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if (canRequest) {
              // the accuracy option will be ignored by iOS
              this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                () => {
                  if (!this.lat) {
                    this.geolocation.getCurrentPosition().then((resp) => {
                      this.lat = resp.coords.latitude;
                      this.lng = resp.coords.longitude;
                      this.loadMap();
                    }).catch((err) => {
                      this._ALERT.presentAlert('Error', err.message);
                    });
                  }
                  else {
                    this.locationSelected = true;
                    this.loadMap();
                  }
                },
                error => {
                  this.lat = -25.30066;
                  this.lng = -57.63591;
                  this.loadMap();
                }
              );
            }
            else {
              this._ALERT.presentAlert('Error', 'Verifica que Debari pueda utilizar tu gps en tus ajustes');
            }
      
          });
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
    );
	

  }

  loadMap() {
    //this._PRELOADER.displayPreloader();
    let latLng = new google.maps.LatLng(this.lat, this.lng);

    let mapOptions = {
	    zoomControl: false,
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      streetViewControl: false
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    //google.maps.event.addListenerOnce(this.map, 'idle', function(){
      this.okButtonEnable = true;
    //});

    google.maps.event.addListener(this.map, 'click', (event) => {
      this.placeMarker(event.latLng);
    });
  }

  placeMarker(location) {
    this.markers.forEach(marker => {
      marker.setMap(null);
    });

    this.markers = [];

    let marker = new google.maps.Marker({
      position: location,
      map: this.map
    });

    this.markers.push(marker);

    this.lat = location.lat();
	this.lng = location.lng();
  }

  selectLocation() {

    this.events.publish('location:selected', { lat: this.lat, lng: this.lng });
    //this.map = null;
    this.navCtrl.pop();
  }

  cancel() {
    this.map = null;
    this.navCtrl.pop();
  }
}
