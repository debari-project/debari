import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  Events,
  ActionSheetController,
  ToastController,
  AlertController,
  MenuController } from 'ionic-angular';

import { AuthProvider } from './../../providers/auth/auth';
import { DatabaseProvider } from '../../providers/database/database';
import { LocalDatabaseProvider } from '../../providers/local-database/local-database';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImageProvider } from '../../providers/image/image';
import { StorageProvider } from '../../providers/storage/storage';
import { AlertProvider } from '../../providers/alert/alert';
import { PhotoViewer } from '@ionic-native/photo-viewer';

import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  currentUser: string;
  uid: string;
  displayName: string;
  email: string;
  photoURL: string;
  creationTime: string;
  range: string = "Novato";
  points: number = 0;
  alertsCount: number = 0;
  public stories: any = []; // definir
  differentUser: boolean = true;
  loadingImage: boolean = false;
  public firstStory: string;
  public readLimit: number = 90;
  public offset: string = null;
  private loading: boolean = true;
  private following: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    private _DB: DatabaseProvider,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    private _IMG: ImageProvider,
    public toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public _LOCALDB: LocalDatabaseProvider,
    public _STORAGE: StorageProvider,
    public _ALERT: AlertProvider,
    private photoViewer: PhotoViewer,
    public menu: MenuController,
    public authData: AuthProvider) {

      this.menu.swipeEnable(false);
      this.uid = this.navParams.get('uid');

  }

  ionViewDidLoad() {
    this.stories = [];
    this.currentUser = this.authData.getUid();

    this._DB.getUserData({ uid: this.uid })
    .then(result => {
      this.displayName = result.displayName;
      this.email = result.email;
      this.photoURL = result.photoURL || 'assets/imgs/default-profile.jpg';
      this.range = result.range || 'Novato';
      this.points = result.points || 0;
      this.alertsCount = result.alertsCount || 0;
      this.creationTime = result.creationTime;

    })
    .catch((err) => {
      this._ALERT.presentAlert('Error', err.message);
    });

    if (this.currentUser == this.uid) {
      this.differentUser = false;

      this._LOCALDB.getUserData()
      .then((result)=>{
        this.photoURL = result.photo_url;
        this.displayName = result.display_name;
        this.email = result.email;
        this.creationTime = result.created_at;
        this.range = result.range;
        this.alertsCount = result.alerts_count;
      })
      .catch((err)=>{
        console.log(err);
      });
    }
    else {
      this._DB.getUserFollowers({uid: this.uid})
      .then((result)=> {
        console.log(result);

        if(result.exists) {
          this.following = true;
        }
      })
      .catch((err)=> {
        console.log(err);
      });
    }

    this.getUserHistory();
  }

  getUserHistory(): Promise<any> {
    let params: any = { uid: this.uid, offset: this.offset };
    return new Promise((resolve) => {
      this._DB.getUserHistory(params)
        .then((data) => {
          if (data.length > 0) {
            this.stories = this.stories.concat(data);
            this.offset = data[data.length - 1].createdAt;
          }

          this.loading = false;
          resolve(true);
        });
    });

  }

  userImageChanged(photoURL) {
    this.events.publish('user:imagechanged', photoURL);
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Selecciona el origen',
      buttons: [
        {
          text: 'Imágenes',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, this.camera.MediaType.PICTURE);
          }
        },
        {
          text: 'Cámara',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA, this.camera.MediaType.PICTURE);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(sourceType, mediaType) {

    this._IMG.takePicture(sourceType, mediaType, true)
      .then((imagePath) => {

        if(imagePath != true) {
          this.loadingImage = true;

          this._STORAGE.uploadUserImage(this.uid, imagePath, 'library')
            .then(snapshot => {
              
                let obj: string;
                obj = snapshot.downloadURL;
  
                this._DB.updateDocument('/users', this.uid, { photoURL: obj })
                  .then((result) => {
                    console.log('ok');
                  })
                  .catch(err => {
                    console.log(err);
                  });
  
                this.authData.updateProfile({photoURL: obj});
                
                this._LOCALDB.updateUserProfile({photoURL: obj})
                  .then(result => {
                  this.userImageChanged(obj);
                  this.photoURL = obj;
                  this.loadingImage = false;
                })
                .catch(err=>{
                  this._ALERT.presentAlert('Error', err.message);
                });
            })
            .catch(err => {
              this._ALERT.presentAlert('Error', err.message);
            });
        }
      }, (err) => {
        console.log(err.message);
      });
  }

  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  viewPhoto(photo) {
    //if img
    this.photoViewer.show(photo, '', { share: true });
    //else
    //video
  }

  doInfinite(infiniteScroll) {
    this.loading = true;
    infiniteScroll.enable(false);

    this.getUserHistory()
      .then(result => {
        infiniteScroll.complete();
        this.loading = false;
        if (this.firstStory != this.stories[0].id) {
          this.firstStory = this.stories[0].id;
          infiniteScroll.enable(true);
        }
      })
      .catch((err)=>{
        this.loading = false;
      });
  }

  refresh(refresher) {
    setTimeout(() => {
      this.offset = null;
      this.stories = [];
      this.getUserHistory();
      refresher.complete();
    }, 2000);
  }

  openStory(story: any) {
    if(story.type == 'alertReceived' || story.type == 'alertSent') {
      this.openPage('ViewAlertPage', { alertId: story.documentId });
    }
    else if(story.type == 'comment') {
      this.openPage('CommentsPage', { alertId: story.alertId, commentId: story.documentId });
    }
  }

  openPage(page: any, params: any) {
    this.navCtrl.push(page, params);
  }

  elapsedTime(pDate): string {
    let date: any = new Date(pDate),
      now: any = new Date(), diff, hours, minutes, seconds, elapsedTime;

    diff = Math.floor(now - date);

    if (diff < 86400000) {
      hours = Math.floor(diff / 3600000);

      if (hours > 0) {
        elapsedTime = hours + " h";
      }
      else
      {

        minutes = Math.floor(diff / 60000);

        if (minutes > 0) {
          elapsedTime = minutes + " min";
        }
        else
        {
          elapsedTime = " un momento";
        }
      }

      elapsedTime = "hace " + elapsedTime;
    }
    else
    {
      let dateString = pDate.toString();
      //elapsedTime = this.formatDate(dateString.substr(0, dateString.indexOf('GMT') - 4));
      elapsedTime = dateString.substr(0, dateString.indexOf('GMT') - 4);
    }
    
    return elapsedTime;
  }

  formatCreationTime(value) {
    moment.locale('es');
    let day = moment(value).date();

    let month = moment(value).format('MMMM');
    let year = moment(value).year();

    return month + " de " + year;
  }

  followUser() {
    this.following = true;

    this._DB.followUser(this.uid)
    .then((result) => {
      console.log('ok');
    })
    .catch((err)=>{
      console.log(err);
      this._ALERT.presentAlert('Error', 'Hubo un error al seguir al usuario, por favor, inténtalo más tarde.');
      this.following = false;
    });
  }

  unFollowUser() {
    this.following = false;

    this._DB.unFollowUser(this.uid)
    .then((result) => {
      console.log('ok');
    })
    .catch((err)=>{
      console.log(err);
      this._ALERT.presentAlert('Error', 'Hubo un error al dejar de seguir al usuario, por favor, inténtalo más tarde.');
      this.following = true;
    }); 
  }
}
