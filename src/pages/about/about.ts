import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AppVersion } from '@ionic-native/app-version';
import { AwaitProvider } from './../../providers/await/await';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  private appName: string;
  private versionNumber: string;

  constructor(private appVersion: AppVersion, private _AWAIT: AwaitProvider) {
  }

  ionViewDidLoad() {
    this.getInfo();
  }

  async getInfo() {
    let err;

    this.appName = await this.appVersion.getAppName();
    this.versionNumber = await this.appVersion.getVersionNumber();
  }
}
