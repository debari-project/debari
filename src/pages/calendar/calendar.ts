import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {
  date: string;
  day: string = moment().format('DD');
  month: string = moment().format('MM');
  year: string = moment().format('YYYY');
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public events: Events) {
    this.date = this.navParams.get("date");
    console.log("constructor: " + this.date);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad: " + this.date);
  }

  changeDate() {
    console.log("chageDate: " + this.date);
	  this.events.publish('date:change', this.date);
	  this.close();
  }

  onDaySelect(ev) {
    this.day = ev.date;
    this.month = ev.month + 1;
    this.year = ev.year;

    this.date = this.pad(this.day) + '/' + this.pad(this.month) + '/' + this.year;
  }
  
  close() {
    this.viewCtrl.dismiss();
  }

  pad(cadena: string) {
    if (cadena.toString().length == 1) {
      cadena = "0" + cadena;
    }
    return cadena;
  }
}
