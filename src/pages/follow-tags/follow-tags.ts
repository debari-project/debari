import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, MenuController } from 'ionic-angular';

import { DatabaseProvider } from './../../providers/database/database';
import { AlertProvider } from './../../providers/alert/alert';
import { LocalDatabaseProvider } from '../../providers/local-database/local-database';

import { FCM } from '@ionic-native/fcm';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-follow-tags',
  templateUrl: 'follow-tags.html',
})
export class FollowTagsPage {
  @ViewChild('chapa') chapa;
  //public chapas: string[] = [];
  public chapas: any = [];
  //public chapa: string;
  public userId: string;

  public myForm: FormGroup;
  public keys: any = [];
  private tagsCount: number = 1;
  private enableFollow: boolean = true;

  loading: boolean = true;
  private notFollowingTags: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public _DB: DatabaseProvider,
    public _ALERT: AlertProvider,
    public _LOCALDB: LocalDatabaseProvider,
    private fcm: FCM,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private menuCtrl: MenuController) {
      this.menuCtrl.swipeEnable(false);

      this.myForm = formBuilder.group({
        control: [{value: '', disabled: false}, Validators.compose([Validators.minLength(6), Validators.maxLength(10), Validators.pattern('[a-zA-Z0-9]*'), Validators.required])]
      });

    this._LOCALDB.getUserData()
      .then(result => {
        this.userId = result.uid;
        this.getTags();
      })
      .catch(err => {
        console.log(err);
      });      
  }

  /*
  ionViewDidEnter() {
    setTimeout(() => {
      console.log(this.chapa);
      this.chapa.setFocus();
      console.log(this.chapa);
      console.log('focus seteado (?)');
    },150);
  }
  */
/*
  ionViewLoaded() {

    setTimeout(() => {
      console.log(this.chapa);
      this.chapa.setFocus();
      console.log(this.chapa);
      console.log('focus seteado (?)');
    },150);

 }*/

  getTags() {

    this._DB.getTags(this.userId)
      .then(result => {
        this.loading = false;
        if (result.length > 0) {
          let temp: any = [];
          this.keys = temp.concat(result);
        }

        if(this.keys.length == 0) {
          this.notFollowingTags = true;
        }

        this.chapa.setFocus();
      })
      .catch(err => {
        this.loading = false;
        this._ALERT.presentAlert('', err.message);
        this.chapa.setFocus();
      });

  }

  addControl(){
    this.tagsCount++;
    this.notFollowingTags = false;
    
    let control = this.myForm.value.control.toUpperCase();

    if (this.keys.indexOf(control) == -1) {
      this.keys.push(control);

      let newTag: any = {};
      let d = new Date();
      newTag[control] = d;

      this.myForm.reset();

      this._DB.setDocument("tags", this.userId, newTag, {merge: true})
      .then((data) => {
        
        let count = this.keys.length;
        if(count == 10) {
          this.enableFollow = false;
        }

      })
      .catch((error) => {
        this._ALERT.presentAlert('Adding document failed', error.message);
      });
    }
    else {
      this._ALERT.presentAlert('', 'Ya estás siguiendo a esta chapa');
    }
  }
  
  removeControl(control){
    let index = this.keys.indexOf(control);
    this.keys.splice(index, 1);

    this._DB.deleteField("tags", this.userId, control)
    .then((data) => {
      this.enableFollow = true;
      this.myForm.reset();
    })
    .catch((error) => {
      this._ALERT.presentAlert('Adding document failed', error.message);
    });
    
    this.presentToast('Se dejó de seguir la chapa ' + control);
  }

  confirmUnfollowTag(tag) {
    let alert = this.alertCtrl.create({
      title: 'Confirmación',
      message: '¿Está seguro que desea dejar de seguir esta chapa?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this.removeControl(tag);
            console.log('tag deleted');
          }
        }
      ]
    });
    alert.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}