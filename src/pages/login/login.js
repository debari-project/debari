var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { AlertProvider } from '../../providers/alert/alert';
import { DatabaseProvider } from '../../providers/database/database';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, menu, formBuilder, authData, alertPrvdr, _DB) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.formBuilder = formBuilder;
        this.authData = authData;
        this.alertPrvdr = alertPrvdr;
        this._DB = _DB;
        this.option = "login";
        this.authData.logoutUser()
            .then(function (result) { console.log('deslogueado'); })
            .catch(function (err) { console.log(err.message); });
        this.menu.swipeEnable(false);
        this.loginForm = formBuilder.group({
            usuario: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(50), Validators.pattern('[a-z0-9_@.]*'), Validators.required])],
            password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(50), Validators.required])]
        });
        this.registForm = formBuilder.group({
            usuario: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(30), Validators.pattern('[a-zA-Z0-9][a-zA-Z0-9_]*'), Validators.required])],
            email: ['', Validators.compose([Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required])],
            password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(50), Validators.required])],
            confirmedPassword: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(50), Validators.required])]
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
    };
    LoginPage.prototype.loginUser = function () {
    };
    LoginPage.prototype.createAccount = function () {
        var _this = this;
        var userId = this.registForm.value.usuario;
        var email = this.registForm.value.email;
        var password = this.registForm.value.password;
        var passwordConfirmation = this.registForm.value.confirmedPassword;
        if (this.isEmail(email)) {
            if (password == passwordConfirmation) {
                this._DB.searchEmail(userId)
                    .then(function (result) {
                    if (result.length) {
                        _this.alertPrvdr.presentAlert('', 'El usuario indicado ya existe, por favor, seleccione otro');
                    }
                    else {
                        /* falta hacer en integro */
                        _this.authData.signupUser(email, password)
                            .then(function (authData) {
                            _this.authData.sendEmailVerification();
                            var userData = {};
                            userData[userId] = { "email": email };
                            _this._DB.insertUser(userData);
                            _this.navCtrl.push('VerifyEmailPage');
                        })
                            .catch(function (err) {
                            console.log(err);
                        });
                    }
                })
                    .catch(function (err) {
                    console.log(err);
                });
            }
            else {
                this.alertPrvdr.presentAlert('', 'Las contraseñas no coinciden');
            }
        }
        else {
            this.alertPrvdr.presentAlert('', 'Por favor, ingrese una cuenta de email válida');
        }
    };
    LoginPage.prototype.isEmail = function (usuario) {
        var re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(usuario);
        if (re) {
            return true;
        }
        else {
            return false;
        }
    };
    return LoginPage;
}());
LoginPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-login',
        templateUrl: 'login.html',
        providers: [AuthProvider, AlertProvider, DatabaseProvider]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        MenuController,
        FormBuilder,
        AuthProvider,
        AlertProvider,
        DatabaseProvider])
], LoginPage);
export { LoginPage };
//# sourceMappingURL=login.js.map