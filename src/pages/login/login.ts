import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  MenuController,
  Events,
  LoadingController,
  Loading,
  TextInput
} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, CheckboxRequiredValidator } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { AlertProvider } from '../../providers/alert/alert';
import { DatabaseProvider } from '../../providers/database/database';
import { StorageProvider } from '../../providers/storage/storage';
import { LocalDatabaseProvider } from '../../providers/local-database/local-database';
import { PreloaderProvider } from '../../providers/preloader/preloader';
import { NetworkProvider } from '../../providers/network/network';
import { AwaitProvider } from '../../providers/await/await';
import { DiagnosticProvider } from './../../providers/diagnostic/diagnostic';
import { GeolocationProvider } from './../../providers/geolocation/geolocation';

import { InAppBrowser } from '@ionic-native/in-app-browser';

// sacar de aca no se como todavia

import * as firebase from 'firebase';
import { GeocodingProvider } from '../../providers/geocoding/geocoding';
import { AndroidPermissionsProvider } from './../../providers/android-permissions/android-permissions';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  @ViewChild('input') myInput: TextInput;
  public option: string = "login";
  public loginForm: FormGroup;
  public registForm: FormGroup;
  public loading: Loading;
  public passwordType: string = 'password';
  public passwordIcon: string = 'ios-eye-off';
  public passwordType2: string = 'password';
  public passwordIcon2: string = 'ios-eye-off';
  public passwordType3: string = 'password';
  public passwordIcon3: string = 'ios-eye-off';
  submitAttempt: boolean = false;
  acceptTermsCheck: boolean = false;

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public formBuilder: FormBuilder,
    public authData: AuthProvider,
    public _ALERT: AlertProvider,
    private _DB: DatabaseProvider,
    public _STORAGE: StorageProvider,
    public events: Events,
    public loadingCtrl: LoadingController,
    public _LOCALDB: LocalDatabaseProvider,
    public _PRELOADER: PreloaderProvider,
    public _NETWORK: NetworkProvider,
    public _AWAIT: AwaitProvider
    ,private iab: InAppBrowser,
    private _DIAGNOSTIC: DiagnosticProvider,
    private _GEOLOCATION: GeolocationProvider,
    private _GEOCODING: GeocodingProvider,
    private _ANDROID_PERMISSIONS: AndroidPermissionsProvider
    ) {

    this.authData.logoutUser()
      .then(result => {
		    console.log('logoutUser');
      })
      .catch(err => {
        console.log(err);
      });

    this.menuCtrl.swipeEnable(false);

    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(30), Validators.required])]
    });

    this.registForm = formBuilder.group({
      firstName: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(30), Validators.pattern('[a-zA-Zá-úÁ-Ú ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(30), Validators.pattern('[a-zA-Zá-úÁ-Ú ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(30), Validators.required])],
      confirmedPassword: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(50), Validators.required])],
      acceptTermsCheck: [this.acceptTermsCheck, Validators.compose([Validators.requiredTrue])]
    });

    this.option = 'registrarse';
    this.option = 'login';
  }
  
  ionViewWillLeave() {
    this._NETWORK.stopNetworkEvents();
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.myInput.setFocus();
    },150);
  }

  async createAccount() {

    if (this._NETWORK.getConnectionType() != 'none') {

      this._PRELOADER.displayPreloader();

      let firstName: string = this.registForm.value.firstName.trim();
      let lastName: string = this.registForm.value.lastName.trim();
      let email: string = this.registForm.value.email.toLowerCase();
      let password: string = this.registForm.value.password;
      let passwordConfirmation: string = this.registForm.value.confirmedPassword;

      if (password == passwordConfirmation) {
        let err, result, geocodeResult, authData, data;

        [err, authData] = await this._AWAIT.to(this.authData.signupUser(email, password));

        if (err) {
          this._PRELOADER.hidePreloader();
          this._ALERT.presentAlert('Error', err.message);
          return Promise.reject(err);
        }

        let completeName: string = firstName + ' ' + lastName;
        let patterns: any = {};
        let names = firstName.split(" ");
        let secondNames = lastName.split(" ");

        names.forEach((name) => {
          patterns[this.removeAccents(name)] = this.removeAccents(completeName);
        });

        secondNames.forEach((secondName) => {
          patterns[this.removeAccents(secondName)] = this.removeAccents(completeName);
        });

        patterns[this.removeAccents(completeName)] = this.removeAccents(completeName);

        let userLocation: any = {};

        [err, result] = await this._AWAIT.to(this.isGpsLocationEnabled());
        if (err) console.log(err);

        if(result) {
          [err, result] = await this._AWAIT.to(this._GEOLOCATION.getCurrentPosition());
          if (err) console.log(err);

          if(result) {
              userLocation = { latitude: result.coords.latitude, longitude: result.coords.longitude };
          } 
        }

        [err, data] = await this._AWAIT.to(this._DB.setDocument('/users/', authData.uid, {
          displayName: completeName,
          displayNameInsensitive: this.removeAccents(completeName),
          patterns: patterns,
          creationTime: firebase.firestore.FieldValue.serverTimestamp(),
          email: email,
          userLocation: userLocation }));

        if (err) {
          this._PRELOADER.hidePreloader();
          this._ALERT.presentAlert('Error', err.message);
          return Promise.reject(err);
        }


       /*[err, data] = await this._AWAIT.to(this._STORAGE.uploadUserImage(authData.uid, "default-profile.jpg", 'local'));

        if (err) {
          this._PRELOADER.hidePreloader();
          this._ALERT.presentAlert('Error', err.message);
          return Promise.reject(err);
        }
        
        [err, data] = await this._AWAIT.to(this.authData.updateProfile({ displayName: completeName }));

        if (err) {
          this._PRELOADER.hidePreloader();
          this._ALERT.presentAlert('Error', err.message);
          return Promise.reject(err);
        }
        
        */

        [err, data] = await this._AWAIT.to(this.authData.sendEmailVerification());

        if (err) {
          this._PRELOADER.hidePreloader();
          this._ALERT.presentAlert('Error', err.message);
          return Promise.reject(err);
        }

        this.option = 'login';
        this.registForm.reset();
        this._PRELOADER.hidePreloader();
        this.navCtrl.push('VerifyEmailPage', { mode: "regist" });

      } else {
        this._ALERT.presentAlert('', 'Las contraseñas no coinciden');
        this._PRELOADER.hidePreloader();
      }

    }
    else {
      this._ALERT.presentAlert('Error', 'No hay conexión a internet');
    }

  }

  login() {

    if (this._NETWORK.getConnectionType() != 'none') {
      
      let email = this.loginForm.value.email;
      let password = this.loginForm.value.password;

      this.authData.loginUser(email, password)
        .then(authData => {
          this.navCtrl.setRoot('HomePage');
        })
        .catch(err => {
          this.submitAttempt = true;
          this.loading.dismiss()
            .then(() => {
              // verificar desloguear si se queda abierta la sesion
              this._ALERT.presentAlert('Error', err.message);
              
            });
        });

      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });

      this.loading.present();
    }
    else {
      this._ALERT.presentAlert('Error', 'No hay conexión a internet.');
    }

  }

  userLoggedIn(usuario: string, email: string, photoURL: string) {
    this.events.publish('user:loggedin', usuario, email, photoURL);
  }

  openPage(page, parameters) {
    this.navCtrl.push(page, parameters);
  }

  hideShowPassword(id) {
    switch (id) {
      case 1:
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'ios-eye-outline' ? 'ios-eye-off' : 'ios-eye-outline';
        break;
      case 2:
        this.passwordType2 = this.passwordType2 === 'text' ? 'password' : 'text';
        this.passwordIcon2 = this.passwordIcon2 === 'ios-eye-outline' ? 'ios-eye-off' : 'ios-eye-outline';
        break;
      case 3:
        this.passwordType3 = this.passwordType3 === 'text' ? 'password' : 'text';
        this.passwordIcon3 = this.passwordIcon3 === 'ios-eye-outline' ? 'ios-eye-off' : 'ios-eye-outline';
        break;
    }
  }

  async loginWithFacebook(): Promise<any> {
    let err, result;

    [err, result] = await this._AWAIT.to(this.authData.signInWithFacebook());
    if (err) {
      this._ALERT.presentAlert('Error', err.message);
      return Promise.reject(err);
    }

    let firstName: string = result.additionalUserInfo.profile.first_name;
    let lastName: string = result.additionalUserInfo.profile.last_name;
    let name: string = result.additionalUserInfo.profile.name;

    let isNewUser: boolean = result.additionalUserInfo.isNewUser;

    let user = result.user;
    let uid: string = user.uid;
    let email: string = user.email;

    if(isNewUser) {
      [err, result] = await this._AWAIT.to(this.authData.updateProfile({emailVerified: true}));

      if (err) {
        this._ALERT.presentAlert('Error', err.message);
        return Promise.reject(err);
      }

      this._STORAGE.uploadUserImage(uid, "default-profile.jpg", 'local');

      this.authData.updateProfile({ displayName: name });

      let completeName: string = name;

      let patterns: any = {};
      let firstNames = firstName.split(" ");
      let secondNames = lastName.split(" ");

      firstNames.forEach((name) => {
        patterns[this.removeAccents(name)] = this.removeAccents(completeName);
      });

      secondNames.forEach((secName) => {
        patterns[this.removeAccents(secName)] = this.removeAccents(completeName);
      });

      let userLocation: any = {};

      [err, result] = await this._AWAIT.to(this.isGpsLocationEnabled());
      if (err) console.log(err);

      if(result) {
        [err, result] = await this._AWAIT.to(this._GEOLOCATION.getCurrentPosition());
        if (err) console.log(err);

        if(result) {
          userLocation = { latitude: result.coords.latitude, longitude: result.coords.longitude };
        } 
      }

      this._DB.setDocument('/users/', uid, {
        displayName: completeName,
        displayNameInsensitive: this.removeAccents(completeName),
        patterns: patterns,
        email: email,
        creationTime: firebase.firestore.FieldValue.serverTimestamp(),
        userLocation: userLocation })
      .then(() => {

        let userObj = { uid: uid, displayName: user.displayName, email: email, photoURL: user.photoURL, range: null, points: 0, alertsCount: 0 };

        this._LOCALDB.saveUserProfile(userObj)
          .then((result) => {
            this.navCtrl.setRoot('HomePage');
          })
          .catch((errLDB) => {
            console.log(errLDB);
          });

      })
      .catch(errUD => {
        console.log(errUD);
      });
            
    } else {

      [err, result] = await this._AWAIT.to(this._DB.getUserData({uid: user.uid}));
      if (err) {
        this._ALERT.presentAlert('ERROR', err.message);
        return Promise.reject(err);
      }

      let userLocation: any = {};
      let range: string;
      let points: number;
      let alertsCount: number;

      range = result.range;
      points = result.points;
      alertsCount = result.alertsCount;
      userLocation = result.userLocation || {};

      this.userLoggedIn(user.displayName, email, user.photoURL);

      [err, result] = await this._AWAIT.to(this.isGpsLocationEnabled());
      if (err) console.log(err);
    
      if(result) {
        [err, result] = await this._AWAIT.to(this._GEOLOCATION.getCurrentPosition());
        if (err) console.log(err);

        if(result) {
            userLocation = { latitude: result.coords.latitude, longitude: result.coords.longitude };

            this._DB.setDocument('/users', user.uid, {userLocation: userLocation}, {merge: true})
            .then(()=>{
              console.log('user location added');
            })
            .catch((err)=>{
              console.log(err);
            });
        } 
      }

      let userObj = { uid: user.uid, displayName: user.displayName, email: user.email, photoURL: user.photoURL, userLocation: userLocation, range: range, points: points, alertsCount: alertsCount };

      this._LOCALDB.saveUserProfile(userObj)
        .then((result) => {
          this.navCtrl.setRoot('HomePage');
        })
        .catch((errLDB) => {
          this._ALERT.presentAlert('ERROR', errLDB.message);
        });
      }

  }

  openPolicies() {
    const browser = this.iab.create('https://www.debari.com.py/politicas/terminos-condiciones.html');
    browser.show()
  }

  removeAccents(s) {
    let r = s.toLowerCase().trim();
    r = r.replace(new RegExp(/\s/g), "");
    r = r.replace(new RegExp(/[àáâãäå]/g), "a");
    r = r.replace(new RegExp(/[èéêë]/g), "e");
    r = r.replace(new RegExp(/[ìíîï]/g), "i");
    r = r.replace(new RegExp(/[òóôõö]/g), "o");
    r = r.replace(new RegExp(/[ùúûü]/g), "u");

    return r;
  }

  async isGpsLocationEnabled(): Promise<any> {
    let err, result;

    [err, result] = await this._AWAIT.to(this._DIAGNOSTIC.isGpsLocationEnabled());
    if (err) return Promise.reject(err);

    return Promise.resolve(result);
  }

}
