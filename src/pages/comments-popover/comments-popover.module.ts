import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentsPopoverPage } from './comments-popover';

@NgModule({
  declarations: [
    CommentsPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(CommentsPopoverPage),
  ],
})
export class CommentsPopoverPageModule {}
