import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database';

import { PhotoViewer } from '@ionic-native/photo-viewer';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';
import { AuthProvider } from './../../providers/auth/auth';

declare var google;

@IonicPage()
@Component({
  selector: 'page-view-alert',
  templateUrl: 'view-alert.html',
})
export class ViewAlertPage {
  @ViewChild('map') mapElement: ElementRef;
  uid: string;
  alertId: string;
  alert: any = [];
  showMap: boolean = false;
  public lat: number;
  public lng: number;
  public locationSelected: string = 'Ninguna seleccionada';

  public alertDate: string;
  public alertHour: string;
  private alertType: string;

  map: any;
  markers: any = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private photoViewer: PhotoViewer,
    private _DB: DatabaseProvider,
    private _AUTH: AuthProvider,
    public menu: MenuController,
    private streamingMedia: StreamingMedia) {
      this.menu.swipeEnable(false);
      this.uid = this._AUTH.getUid();
      this.alertId = this.navParams.get('alertId');
      this.alert = {alertDate: "", chapasNo: [], registDate: "", images: [], userImg: "", description: "", displayName: "", location: ""};

      console.log('constructor');
    if(this.alertId == null) {
      this.alert = this.navParams.get('alert');

      console.log(this.alert);
      this.alertDate = this.alert.alertDate.substring(0, 10);
      this.alertHour = this.alert.alertDate.substring(this.alert.alertDate.length - 5);

      if (this.alert.location != undefined) {
        this.lat = this.alert.location.lat;
        this.lng = this.alert.location.lng;
        this.locationSelected = this.lat + ',' + this.lng;
      }
    }
    else {
      this._DB.getAlert(this.alertId, this.uid)
      .then((result)=>{
        this.alert = result;
        this.alertDate = this.alert.alertDate.substring(0, 10);
        this.alertHour = this.alert.alertDate.substring(this.alert.alertDate.length - 5);

        if (this.alert.location != undefined) {
          this.lat = this.alert.location.lat;
          this.lng = this.alert.location.lng;
          this.locationSelected = this.lat + ',' + this.lng;
        }
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewAlertPage');
  }

  ionViewWillEnter() {
    if(this.lat != null) {
      this.showMap = true;
      this.loadMap();
    }
  }


  elapsedTime(pDate): string {
    let date: any = new Date(pDate),
      now: any = new Date(), diff, hours, minutes, seconds, elapsedTime;

    diff = Math.floor(now - date);

    if (diff < 86400000) {
      hours = Math.floor(diff / 3600000);

      if (hours > 0) {
        elapsedTime = hours + " h";
      }
      else
      {

        minutes = Math.floor(diff / 60000);

        if (minutes > 0) {
          elapsedTime = minutes + " min";
        }
        else
        {
          elapsedTime = " un momento";
        }
      }

      elapsedTime = "hace " + elapsedTime;
    }
    else
    {
      let dateString = pDate.toString();
      //elapsedTime = this.formatDate(dateString.substr(0, dateString.indexOf('GMT') - 4));
      elapsedTime = dateString.substr(0, dateString.indexOf('GMT') - 4);
    }
    
    return elapsedTime;
  }
  
  loadNextImage(alertId: string) {
    //let y = this.alerts.find(x => x.id == alertId);
    //let x = y.images.find(x => x.visible == 0);
    //x.visible=1;
  }

  currentUserPost(userId: string): boolean {
    //if (userId == this.uid) {
      return true;
    /*}
    else
    {
      return false;
    }*/
  }


  viewFile(file) {

    if (!file.thumbnail) {
      this.photoViewer.show(file.src, '', { share: true });
    }
    else {
      let options: StreamingVideoOptions = {
        successCallback: () => { console.log('Video played') },
        errorCallback: (e) => { console.log('Error streaming') },
        shouldAutoClose: true
      };
      
      this.streamingMedia.playVideo(file.src, options);
    }
  }

  openPage(component) {
    this.showMap = false;
    this.navCtrl.push(component, { lat: this.lat, lng: this.lng });
  }

  loadMap() {
    console.log('loadmap');
    let latLng = new google.maps.LatLng(this.lat, this.lng);

    let mapOptions = {
	  zoomControl: false,
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
	    scaleControl: false
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.placeMarker(latLng);
  }

  placeMarker(location) {
    this.markers.forEach(marker => {
      marker.setMap(null);
    });

    this.markers = [];

    let marker = new google.maps.Marker({
      position: location,
      map: this.map
    });

    this.markers.push(marker);
  }
}
