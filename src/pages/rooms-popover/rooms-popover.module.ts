import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoomsPopoverPage } from './rooms-popover';

@NgModule({
  declarations: [
    RoomsPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(RoomsPopoverPage),
  ],
})
export class RoomsPopoverPageModule {}
