import { Component } from '@angular/core';
import {
  App,
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  Events,
  ViewController,
  PopoverController,
  ModalController
} from 'ionic-angular';

import { AlertProvider } from '../../providers/alert/alert';
import { AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {

  public options: any = [];

  constructor(
    public viewCtrl: ViewController,
    private navParams: NavParams,
    private navCtrl: NavController,
    private _ALERT: AlertProvider,
    private alertCtrl: AlertController,
    private _DB: DatabaseProvider,
    public toastCtrl: ToastController,
    public events: Events,
    private popoverCtrl: PopoverController,
    private modalCtrl: ModalController,
	public appCtrl: App
  ) {

  }

  ngOnInit() {
    if (this.navParams.data) {
      this.options = this.navParams.data.options;
    }
  }

  makeOption(option) {
    if (option.optionName != "Eliminar") {
      //this._ALERT.presentAlert('alert.id', option.alert.id);
	  this.close();
	  this.appCtrl.getRootNav().push(option.component, { alertId: option.alertId });
    }
    else {
      this.confirmDeleteAlert(option.alert.id);
	  this.close();
    }
    
  }

  confirmDeleteAlert(alertId) {
    let alert = this.alertCtrl.create({
      title: 'Confirmación',
      message: '¿Está seguro que desea eliminar esta alerta?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this._DB.deleteDocument('/alerts', alertId)
              .then(result => {

                this.presentToast('Alerta eliminada');
                this.events.publish('alert:deleted', alertId);
                this.navCtrl.pop();
              })
              .catch(err => {
                this._ALERT.presentAlert('Error', err.message);
              });
          }
        }
      ]
    });
    alert.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  close() {
    this.viewCtrl.dismiss();
  }

  presentModal(page, params) {
    let modal = this.modalCtrl.create(page, params);
    modal.present();
  }
}
