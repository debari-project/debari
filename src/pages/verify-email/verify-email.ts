import { Component } from '@angular/core';
import {
  IonicPage, NavController, NavParams,
  Events } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { AlertProvider } from '../../providers/alert/alert';

/**
 * Generated class for the VerifyEmailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify-email',
  templateUrl: 'verify-email.html',
})
export class VerifyEmailPage {
  public mode: string;
  public title: string;
  public email: string;
  public connection: boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public authData: AuthProvider,
    public alertPrvdr: AlertProvider) {

    this.mode = this.navParams.get("mode");
    this.email = this.navParams.get("email");

    if (this.mode == "regist") {
      this.title = "¡Gracias por unirte!";
    }
    else if (this.mode == "reset"){
      this.title = "Reseteo de contraseña";
    }
  }

  sendEmailVerification() {

    if (this.mode == "regist") {
      this.authData.sendEmailVerification()
    } else if (this.mode == "reset") {

          this.authData.resetPassword(this.email)
            .then(resetResult => {
              //this.loading.dismiss();
            });
    }

      this.alertPrvdr.presentAlert('', 'Verificacion reenviada');
  }

}
