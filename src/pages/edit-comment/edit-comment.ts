import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-edit-comment',
  templateUrl: 'edit-comment.html',
})
export class EditCommentPage {
  private commentObj: any;
  private comment: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.commentObj = this.navParams.get('comment');
    this.comment = this.commentObj.comment;
  }

  ionViewDidLoad() {

  }

}
